import { Request, Response, NextFunction, Application } from 'express';
import { BaseRoute } from './base-route';
import { Logger } from '../helpers/helpers';

export class SampleRouter extends BaseRoute {

    constructor(private app: Application) {
        super();
        this.init();
    }

    public sample(req: Request, res: Response, next: NextFunction) {
        try {
            super.PerformGetRequest("", {}, (data) => {
                res.send(data);
            });
        }
        catch (e) {
            Logger.error(e);
        }
    }

    init() {
        this.app.post('/api/sample', this.sample);
    }
}